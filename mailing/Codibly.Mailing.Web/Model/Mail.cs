﻿namespace Codibly.Mailing.Web.Model
{
    public class Mail
    {
        public string Subject { get; set; }

        public string Sender { get; set; }

        public string Recipients { get; set; }

        public string Body { get; set; }
    }
}
