﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Codibly.Mailing.Web.Model
{
    public class SmtpSettings
    {
        public string SmtpAddress { get; set; }

        public int SmtpPort { get; set; }

        public string SmtpUser { get; set; }

        public string SmtpPassword { get; set; }
    }
}
