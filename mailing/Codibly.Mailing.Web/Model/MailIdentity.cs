﻿using System;

namespace Codibly.Mailing.Web.Model
{
    public class MailIdentity
    {
        public Guid Guid { get; set; }
    }
}
