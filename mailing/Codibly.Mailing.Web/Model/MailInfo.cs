﻿using Codibly.Mailing.Model;

namespace Codibly.Mailing.Web.Model
{
    public class MailInfo
    {
        public MailIdentity Identity { get; set; }

        public MailStatus Status { get; set; }

        public string Header { get; set; }
    }
}
