﻿using System;

namespace Codibly.Mailing.Web.Model
{
    public class UserIdentity
    {
        public Guid Guid { get; set; }
    }
}
