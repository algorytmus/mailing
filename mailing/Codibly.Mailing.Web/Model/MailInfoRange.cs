﻿namespace Codibly.Mailing.Web.Model
{
    public class MailInfoPage
    {
        public MailInfo[] Mails { get; set; }

        public int TotalPages { get; set; }
    }
}
