using Codibly.Mailing.Model;
using Codibly.Mailing.Outgoing;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Codibly.Mailing.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerDocument();
            services.AddSingleton<IMailingContextProvider>(s => new InMemoryMailingContextProvider());
            services.AddSingleton(s => s.GetService<IMailingContextProvider>().GetMailingContext());
            services.AddSingleton<IMailingRepository>(s => new MailingRepository(s.GetService<MailingContext>()));
            services.AddSingleton<IMailSender>(new MailSender());
            services.AddSingleton<IOutgoingQueue>(s => new OutgoingQueue(s.GetService<IMailingRepository>(), s.GetService<IMailSender>()));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UsePathBase(new PathString("/api"));

            app.UseHttpsRedirection();

            app.UseOpenApi();

            app.UseSwaggerUi3();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
