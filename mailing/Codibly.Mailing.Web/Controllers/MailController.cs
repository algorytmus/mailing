﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Codibly.Mailing.Model;
using Codibly.Mailing.Outgoing;
using Codibly.Mailing.Web.Model;
using Microsoft.AspNetCore.Mvc;

namespace Codibly.Mailing.Web.Controllers
{
    [ApiController]
    public class MailController : ControllerBase
    {
        private readonly IMailingRepository mailingRepository;
        private readonly IOutgoingQueue outgoingQueue;

        public MailController([NotNull] IMailingRepository mailingRepository, [NotNull] IOutgoingQueue outgoingQueue)
        {
            this.mailingRepository = mailingRepository;
            this.outgoingQueue = outgoingQueue;
        }

        [HttpGet("users/{userId:Guid}/mails/{pageIndex}/{pageSize}")]
        public MailInfoPage Range([FromRoute] Guid userId, [FromRoute] int pageIndex, [FromRoute] int pageSize)
        {
            var count = mailingRepository.GetMailCount(userId);
            var totalPages = Math.DivRem(count, pageSize, out int remainder) + (remainder > 0 ? 1 : 0);
            return new MailInfoPage
            {
                Mails = mailingRepository
                    .GetMailRange(userId, (pageIndex - 1) * pageSize, pageSize)
                    .Select(ConvertToInfo)
                    .ToArray(),
                TotalPages = totalPages
            };
        }
        
        [HttpPost("users/{userId:Guid}/draft-mails")]
        public MailIdentity Create([FromRoute] Guid userId, [FromBody] Mail mail)
        {
            var mailEntity = mailingRepository.CreateMail(userId, mailEntity =>
            {
                mailEntity.Body = mail.Body;
                mailEntity.Recipients = mail.Recipients;
                mailEntity.Sender = mail.Sender;
                mailEntity.Status = MailStatus.Saved;
                mailEntity.Subject = mail.Subject;
            });
            return new MailIdentity { Guid = mailEntity.Id };
        }

        [HttpGet("mails/{mailId:Guid}/status")]
        public MailStatus GetStatus([FromRoute] Guid mailId)
        {
            return GetMailEntity(mailId).Status;
        }

        [HttpPost("mails/{mailId:Guid}/actions/send")]
        public void Send([FromRoute] Guid mailId)
        {
            outgoingQueue.Enqueue(mailId);
        }
        
        [HttpGet("mails/{mailId:Guid}")]
        public Mail GetMail([FromRoute] Guid mailId)
        {
            return Convert(GetMailEntity(mailId));
        }

        [HttpPut("mails/{mailId:Guid}")]
        public void SetMail([FromRoute] Guid mailId, [FromBody] Mail mail)
        {
            mailingRepository.UpdateMail(mailId, mailEntity => 
            {
                mailEntity.Body = mail.Body;
                mailEntity.Recipients = mail.Recipients;
                mailEntity.Sender = mail.Sender;
                mailEntity.Subject = mail.Subject;
                mailEntity.Status = MailStatus.Saved;
            });
        }

        [HttpPost("users/{userId:Guid}/draft-mails/actions/send-all")]
        public void SendAll([FromRoute] Guid userId)
        {
            foreach(var mailId in mailingRepository.GetMails(userId, MailStatus.Saved, MailStatus.Error /* Resend */))
            {
                outgoingQueue.Enqueue(mailId);
            }
        }

        private Mail Convert(MailEntity mail)
        {
            return new Mail
            {
                Body = mail.Body,
                Subject = mail.Subject,
                Sender = mail.Sender,
                Recipients = mail.Recipients
            };
        }

        private MailInfo ConvertToInfo(MailEntity mail)
        {
            return new MailInfo
            {
                Header = mail.Subject.Substring(0, Math.Min(20, mail.Subject.Length)),
                Status = mail.Status,
                Identity = new MailIdentity { Guid = mail.Id }
            };
        }

        private MailEntity GetMailEntity(Guid mailId)
        {
            return mailingRepository.GetMail(mailId);
        }
    }
}
