﻿using System;
using System.Diagnostics.CodeAnalysis;
using Codibly.Mailing.Model;
using Codibly.Mailing.Web.Model;
using Microsoft.AspNetCore.Mvc;

namespace Codibly.Mailing.Web.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMailingRepository mailingRepository;

        public UserController([NotNull] IMailingRepository mailingRepository)
        {
            this.mailingRepository = mailingRepository;
        }

        [HttpPost("users")]
        public UserIdentity Create()
        {
            var user = mailingRepository.CreateUser();
            return new UserIdentity { Guid = user.Id };
        }

        [HttpPut("users/{userId}/smtp-settings")]
        public void SetSmtpSettings([FromRoute] Guid userId, [FromBody] SmtpSettings smtpSettings)
        {
            mailingRepository.UpdateUserSettings(userId, user =>
            {
                user.SmtpAddress = smtpSettings.SmtpAddress;
                user.SmtpPassword = smtpSettings.SmtpPassword;
                user.SmtpPort = smtpSettings.SmtpPort;
                user.SmtpUser = smtpSettings.SmtpUser;
            });
        }

        [HttpGet("users/{userId}/smtp-settings")]
        public SmtpSettings GetSmtpSettings([FromRoute] Guid userId)
        {
            var user = mailingRepository.GetUser(userId);
            return new SmtpSettings
            {
                SmtpAddress = user.SmtpAddress,
                SmtpPassword = string.Empty,
                SmtpPort = user.SmtpPort,
                SmtpUser = user.SmtpUser
            };
        }
    }
}
