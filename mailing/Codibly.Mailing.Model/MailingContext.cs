﻿using Microsoft.EntityFrameworkCore;

namespace Codibly.Mailing.Model
{
    public class MailingContext : DbContext
    {
        public DbSet<MailEntity> Mails { get; set; }
        public DbSet<UserEntity> Users { get; set; }

        public MailingContext(DbContextOptions<MailingContext> options)
        : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MailEntity>()
                .HasOne(m => m.User)
                .WithMany(u => u.Mails);
        }
    }
}
