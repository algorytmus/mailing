﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codibly.Mailing.Model
{
    public interface IMailingRepository
    {
        UserEntity CreateUser();
        void UpdateUserSettings(Guid userId, Action<UserEntity> factory);

        UserEntity GetUser(Guid userId);

        int GetMailCount(Guid userId);

        IReadOnlyList<MailEntity> GetMailRange(Guid userId, int indexFrom, int count);

        IReadOnlyList<Guid> GetMails(Guid userId, params MailStatus[] mailStatus);

        MailEntity CreateMail(Guid userId, Action<MailEntity> factory);

        MailEntity GetMail(Guid mailId);

        void UpdateMail(Guid mailId, Action<MailEntity> update);
    }

    public class MailingRepository : IMailingRepository
    {
        private readonly MailingContext mailingContext;

        public MailingRepository(MailingContext mailingContext)
        {
            this.mailingContext = mailingContext;
        }

        public MailEntity CreateMail(Guid userId, Action<MailEntity> factory)
        {
            var user = mailingContext.Users.Find(userId);
            var mailEntity = MailEntity.CreateNew(user);
            factory(mailEntity);
            mailingContext.Mails.Add(mailEntity);
            mailingContext.SaveChanges();
            return mailEntity;
        }

        public UserEntity CreateUser()
        {
            var user = UserEntity.CreateNew();
            mailingContext.Users.Add(user);
            mailingContext.SaveChanges();
            return user;
        }

        public MailEntity GetMail(Guid mailId)
        {
            return mailingContext.Mails.Find(mailId);
        }

        public int GetMailCount(Guid userId)
        {
            return mailingContext.Mails.Count();
        }

        public IReadOnlyList<MailEntity> GetMailRange(Guid userId, int indexFrom, int count)
        {
            return mailingContext
                    .Mails
                    .Where(m => m.User.Id == userId)
                    .Skip(indexFrom)
                    .Take(count)
                    .ToArray();
        }

        public IReadOnlyList<Guid> GetMails(Guid userId, params MailStatus[] mailStatus)
        {
            return mailingContext
                .Mails
                .Where(mail => mail.User.Id == userId && mailStatus.Contains(mail.Status))
                .Select(mail => mail.Id)
                .ToArray();
        }

        public UserEntity GetUser(Guid userId)
        {
            return mailingContext.Users.Find(userId);
        }

        public void UpdateMail(Guid mailId, Action<MailEntity> update)
        {
            var mailEntity = mailingContext.Mails.Find(mailId);
            update(mailEntity);
            mailingContext.Mails.Update(mailEntity);
            mailingContext.SaveChanges();
        }

        public void UpdateUserSettings(Guid userId, Action<UserEntity> update)
        {
            var user = mailingContext.Users.Find(userId);
            update(user);
            mailingContext.Update(user);
            mailingContext.SaveChanges();
        }
    }
}
