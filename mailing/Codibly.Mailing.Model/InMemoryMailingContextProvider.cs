﻿using Microsoft.EntityFrameworkCore;

namespace Codibly.Mailing.Model
{
    public class InMemoryMailingContextProvider : IMailingContextProvider
    {
        public MailingContext GetMailingContext()
        {
            var builder = new DbContextOptionsBuilder<MailingContext>()
                .UseInMemoryDatabase(databaseName: "codibly-mails-db");
            return new MailingContext(builder.Options);
        }
    }
}
