﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Codibly.Mailing.Model
{
    public class UserEntity
    {
        [Key]
        public Guid Id { get; internal set; }

        public string SmtpAddress { get; set; } = string.Empty;

        public int SmtpPort { get; set; }

        public string SmtpUser { get; set; } = string.Empty;

        public string SmtpPassword { get; set; } = string.Empty;

        public List<MailEntity> Mails { get; set; } = new List<MailEntity>();

        public static UserEntity CreateNew()
            => new UserEntity { Id = Guid.NewGuid() };
    }
}
