﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Codibly.Mailing.Model
{
    public class MailEntity
    {
        [Key]
        public Guid Id { get; internal set; }

        public UserEntity User { get; internal set; }

        public MailStatus Status { get; set; }

        public string Subject { get; set; } = string.Empty;

        public string Sender { get; set; } = string.Empty;

        public string Recipients { get; set; } = string.Empty;

        public string Body { get; set; } = string.Empty;

        public static MailEntity CreateNew(UserEntity owner)
            => new MailEntity()
            {
                Id = Guid.NewGuid(),
                User = owner
            };
    }
}
