﻿namespace Codibly.Mailing.Model
{
    public enum MailStatus
    {
        Saved,
        Sending,
        Sent,
        Error
    }
}
