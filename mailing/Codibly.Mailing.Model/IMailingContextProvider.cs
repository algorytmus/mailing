﻿namespace Codibly.Mailing.Model
{
    public interface IMailingContextProvider
    {
        MailingContext GetMailingContext();
    }
}
