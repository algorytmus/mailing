﻿using Codibly.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Codibly.Mailing.Outgoing
{
    public interface IOutgoingQueue
    {
        public void Enqueue(Guid mailId);
    }

    public class OutgoingQueue : IOutgoingQueue
    {
        private readonly IMailSender mailSender;
        private readonly IMailingRepository mailingRepository;
        private readonly HashSet<Guid> hashSet = new HashSet<Guid>();

        public OutgoingQueue([NotNull] IMailingRepository mailingRepository, [NotNull] IMailSender mailSender)
        {
            this.mailSender = mailSender;
            this.mailingRepository = mailingRepository;
        }

        public void Enqueue(Guid mailId)
        {
            lock(this)
            {
                if(hashSet.Contains(mailId))
                {
                    return;
                }
                hashSet.Add(mailId);
                Task.Run(() =>
                   {
                       UpdateStatus(mailId, MailStatus.Sending);
                       Task.Run(() =>
                       {
                           Send(mailId);
                       });
                   });
            }
        }

        private void Send(Guid mailId)
        {
            var toSend = mailingRepository.GetMail(mailId);
            var result = mailSender.Send(toSend);
            UpdateStatus(mailId, result ? MailStatus.Sent : MailStatus.Error);
            lock (this)
            {
                hashSet.Remove(mailId);
            }
        }

        private void UpdateStatus(Guid mailId, MailStatus status)
        {
            lock (mailingRepository)
            {
                mailingRepository.UpdateMail(mailId, mail => mail.Status = status);
            }
        }
    }
}
