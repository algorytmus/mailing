﻿using Codibly.Mailing.Model;
using System.Diagnostics.CodeAnalysis;
using System.Net.Mail;
using System;

namespace Codibly.Mailing.Outgoing
{
    public interface IMailSender
    {
        bool Send([NotNull] MailEntity entity);
    }
    public class MailSender : IMailSender
    {
        public bool Send([NotNull] MailEntity entity)
        {
            try{
                var client = new SmtpClient(entity.User.SmtpAddress);
                client.Credentials = new System.Net.NetworkCredential(entity.User.SmtpUser, entity.User.SmtpPassword);
                var message = new MailMessage(entity.Sender, entity.Recipients)
                {
                    Body = entity.Body,
                    Subject = entity.Subject
                };
                client.Send(message);
                return true;
            }catch
            {
                //TODO: Exception not handled properly
                return false;
            }
        }
    }
}
