using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Codibly.Mailing.Web.Client.Services;
using Blazored.LocalStorage;
using System.Net.Http;
using Codibly.Mailing.Web.Client.ServiceReferences;

namespace Codibly.Mailing.Web.Client
{
    public class Startup
    {
        private const string MailingServiceBaseUrlKey = "MailingServiceBaseUrl";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private bool isDevelopment;

        public string MailingServiceBaseUrl
            => isDevelopment 
            ? "https://codiblymailingweb20200311024711.azurewebsites.net/" 
            : Configuration[MailingServiceBaseUrlKey];

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddBlazoredLocalStorage();
            services.AddScoped<HttpClient>();
            services.AddScoped(sc => new MailClient(MailingServiceBaseUrl, sc.GetRequiredService<HttpClient>()));
            services.AddScoped(sc => new UserClient(MailingServiceBaseUrl, sc.GetRequiredService<HttpClient>()));
            services.AddScoped<UserContextProvider>();
            services.AddScoped<Mailbox>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            isDevelopment = env.IsDevelopment();
            if (isDevelopment)
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
