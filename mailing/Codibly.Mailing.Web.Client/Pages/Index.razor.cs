﻿using Codibly.Mailing.Web.Client.ServiceReferences;
using Codibly.Mailing.Web.Client.Services;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;

namespace Codibly.Mailing.Web.Client.Pages
{
    public class EditSmtpSettingsViewModel : ComponentBase
    {
        private SmtpSettings smtpSettings;

        [Inject]
        protected Mailbox Mailbox { get; set; }

        public string SmtpAddress
        {
            get => smtpSettings?.SmtpAddress ?? string.Empty;
            set
            {
                if (smtpSettings != null)
                {
                    smtpSettings.SmtpAddress = value;
                }
            }
        }

        public string SmtpPort
        {
            get => smtpSettings?.SmtpPort.ToString() ?? string.Empty;
            set
            {
                if (smtpSettings != null)
                {
                    int.TryParse(value, out var port);
                    smtpSettings.SmtpPort = port;
                }
            }
        }

        public string SmtpUser
        {
            get => smtpSettings?.SmtpUser ?? string.Empty;
            set
            {
                if (smtpSettings != null)
                {
                    smtpSettings.SmtpUser = value;
                }
            }
        }

        public string SmtpPassword
        {
            get => smtpSettings?.SmtpPassword ?? string.Empty;
            set
            {
                if(smtpSettings != null)
                {
                    smtpSettings.SmtpPassword = value;
                }
            }
        }

        public async Task Save()
            => await Mailbox.SetSmtpSettings(smtpSettings);

        protected bool IsLoading 
            => smtpSettings == null;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if(smtpSettings == null)
            {
                smtpSettings = await Mailbox.GetSmtpSettings();
                StateHasChanged();
            }
            await base.OnAfterRenderAsync(firstRender);
        }
    }
}
