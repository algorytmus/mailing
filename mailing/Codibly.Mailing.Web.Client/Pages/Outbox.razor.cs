﻿using Codibly.Mailing.Web.Client.ServiceReferences;
using Codibly.Mailing.Web.Client.Services;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Reactive.Linq;

namespace Codibly.Mailing.Web.Client.Pages
{
    public class OutboxViewModel : ComponentBase
    {
        private const int PageSize = 10;
        private IReadOnlyList<MailInfo> mails;

        [Inject]
        protected Mailbox Mailbox { get; set; }

        public IReadOnlyList<MailInfo> Mails
            => mails ?? Enumerable.Empty<MailInfo>().ToList();

        public int TotalPages { get; private set; } = 1;

        public int PageIndex { get; private set; } = 1;

        public bool IsLoading { get; private set; }

        public async Task GoToPage(int pageIndex)
        {
            await LoadPage(pageIndex);
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (mails == null)
            {
                IsLoading = true;
                StateHasChanged();
                await LoadPage(PageIndex);
                IsLoading = false;
                StateHasChanged();
            }
            base.OnAfterRender(firstRender);
        }

        private async Task LoadPage(int pageIndex)
        {
            var page = await Mailbox.GetMails(pageIndex, PageSize);
            TotalPages = page.TotalPages;
            mails = page.Mails.ToList();
            PageIndex = pageIndex;
        }

        protected async Task SendAll()
        {
            IsLoading = true;
            StateHasChanged();
            await Mailbox.SendAll();
            await LoadPage(PageIndex);
            IsLoading = false;
            StateHasChanged();
        }
    }
}
