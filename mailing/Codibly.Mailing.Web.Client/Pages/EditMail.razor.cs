﻿using Codibly.Mailing.Web.Client.ServiceReferences;
using Codibly.Mailing.Web.Client.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Codibly.Mailing.Web.Client.Pages
{
    public class EditMailViewModel : ComponentBase
    {
        private const string SenderRegex = @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$";
        private const string RecipientsRegex = @"^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},?)+$";

        [Inject]
        protected Mailbox Mailbox { get; set; }

        [Parameter]
        public Guid? MailId { private get; set; }

        public string Status 
            => MailId.HasValue ? ToString(MailStatus) : "Draft";

        public MailStatus MailStatus { get; private set; }

        public string Subject { get; set; }

        [RegularExpression(SenderRegex, 
            ErrorMessage = "Sender address not valid")]
        [Required]
        public string Sender { get; set; } = string.Empty;

        [RegularExpression(RecipientsRegex, 
            ErrorMessage = "Recipient addresses not valid")]
        [Required]
        public string Recipients { get; set; } = string.Empty;

        public string Body { get; set; } = string.Empty;

        public string Error { get; set; } = string.Empty;

        public async Task Save()
        {
            await InvokeWithLoading(() => SaveInternal());
            StateHasChanged();
        }

        protected bool IsLoading { private set; get; }

        private async Task SaveInternal()
        {
            if (MailId == null)
            {
                MailId = (await Mailbox.Create(GetContent())).Guid;
                MailStatus = MailStatus.Saved;
            }
            else
            {
                await Mailbox.Save(ToMailIdentity(MailId.Value), GetContent());
            }
        }

        protected override async Task OnInitializedAsync()
        {
            if (MailId.HasValue)
            {
                await InvokeWithLoading(() => Load());
            }
            await base.OnInitializedAsync();
        }

        private async Task Load()
        {
            await ReadStatus();
            SetContent(await Mailbox.GetMail(ToMailIdentity(MailId.Value)));
        }

        private async Task ReadStatus()
            => MailStatus = await Mailbox.GetStatus(ToMailIdentity(MailId.Value));

        private void SetContent(Mail mail)
        {
            Body = mail.Body;
            Subject = mail.Subject;
            Recipients = mail.Recipients;
            Sender = mail.Sender;
        }

        private Mail GetContent()
            => new Mail
            {
                Body = Body ?? string.Empty,
                Subject = Subject ?? string.Empty,
                Recipients = Recipients ?? string.Empty,
                Sender = Sender ?? string.Empty
            };

        public async Task Send()
        {
            await InvokeWithLoading(() => SendInternal());
            await InvokeWithLoading(async () => { await SyncStatus(); StateHasChanged(); });
        }

        public async Task SendInternal()
        {
            MailStatus = MailStatus.Sending;
            await SaveInternal();
            await Mailbox.Send(ToMailIdentity(MailId.Value));
        }

        private const int SyncStatusDelay = 3000;
        private async Task SyncStatus()
        {
            await Task.Delay(SyncStatusDelay);
            await ReadStatus();
            if (MailStatus == MailStatus.Sending)
            {
                await SyncStatus();
            }
        }

        public bool SendDisabled
            => !IsSenderValid 
            || !AreRecipientsValid 
            || IsLoading;

        public bool SaveDisabled 
            => MailStatus == MailStatus.Sent 
            || MailStatus == MailStatus.Sending
            || IsLoading;

        private bool IsSenderValid
            => new Regex(SenderRegex).IsMatch(Sender);

        private bool AreRecipientsValid
            => new Regex(RecipientsRegex).IsMatch(Recipients);

        private static MailIdentity ToMailIdentity(Guid guid)
            => new MailIdentity { Guid = guid };

        private static string ToString(MailStatus mailStatus)
            => Enum.GetName(typeof(MailStatus), mailStatus);

        private async Task InvokeWithLoading(Func<Task> action)
        {
            Error = string.Empty;
            IsLoading = true;
            try
            {
                await action();
            }
            catch(Exception ex)
            {
                Error = ex.Message;
            }
            IsLoading = false;
        }
    }
}
