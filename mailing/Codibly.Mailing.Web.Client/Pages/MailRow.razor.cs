﻿using Codibly.Mailing.Web.Client.ServiceReferences;
using Codibly.Mailing.Web.Client.Services;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using System.Linq;
using ReactiveUI;
using System;
using System.Reactive;
using System.Reactive.Linq;

namespace Codibly.Mailing.Web.Client.Pages
{
    public class MailInfoViewModel : ComponentBase
    {
        private static readonly TimeSpan StatusRefreshInternval = TimeSpan.FromSeconds(2);

        private ObservableAsPropertyHelper<MailStatus> mailStatus;
        private ReactiveCommand<Unit, Unit> send;

        [Inject]
        protected Mailbox Mailbox { get; set; }

        [Parameter]
        public MailInfo MailInfo { get; set; }

        protected override void OnInitialized()
        {
            Header = MailInfo.Header;
            Id = MailInfo.Identity.Guid;
            send = CreateSendCommand(MailInfo, Mailbox);
            var onSend = send.IsExecuting.Where(isExecuting => isExecuting).Select(_ => Unit.Default);
            mailStatus = CreateStatusProperty(MailInfo, Mailbox, StateHasChanged, onSend);
            base.OnInitialized();
        }

        private static ObservableAsPropertyHelper<MailStatus> CreateStatusProperty
            (MailInfo mailInfo, Mailbox mailbox,
                Action onStatusUpdate,
                IObservable<Unit> onSend)
        {
            var mailStatuses = Observable.Interval(StatusRefreshInternval)
                .ManySelect(_ => Observable.FromAsync(() => mailbox.GetStatus(mailInfo.Identity)))
                .Merge()
                .Merge(onSend.Select(_ => MailStatus.Sending))
                .DistinctUntilChanged();
            return new ObservableAsPropertyHelper<MailStatus>(mailStatuses, _ => onStatusUpdate(), mailInfo.Status, true);
        }

        private static ReactiveCommand<Unit, Unit> CreateSendCommand(MailInfo mailInfo, Mailbox mailbox)
        {
            return ReactiveCommand.CreateFromTask(async () => await mailbox.Send(mailInfo.Identity));
        }

        [Parameter]
        public EventCallback<MailStatus> StatusUpdated { get; set; }

        public string Header { get; private set; }

        public MailStatus Status
            => mailStatus.Value;

        public bool SendDisabled
            => Status == MailStatus.Sending
            || Status == MailStatus.Sent;

        public Guid Id { get; private set; }

        public async Task Send()
            => await send.Execute();
    }
}
