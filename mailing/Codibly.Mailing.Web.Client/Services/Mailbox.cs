﻿using Codibly.Mailing.Web.Client.ServiceReferences;
using System;
using System.Threading.Tasks;

namespace Codibly.Mailing.Web.Client.Services
{
    public class Mailbox
    {
        private readonly MailClient mailClient;
        private readonly UserClient userClient;
        private readonly Lazy<Task<UserIdentity>> user;

        public Mailbox(UserContextProvider userContext, MailClient mailClient, UserClient userClient)
        {
            this.mailClient = mailClient;
            this.userClient = userClient;
            user = new Lazy<Task<UserIdentity>>(async () => await userContext.GetUser());
        }

        public async Task<MailInfoPage> GetMails(int pageIndex, int pageSize)
            => await mailClient.RangeAsync(await GetUser(), pageIndex, pageSize);

        public async Task Send(MailIdentity mailId)
            => await mailClient.SendAsync(mailId.Guid);

        public async Task<MailIdentity> Create(Mail mail)
            => await mailClient.CreateAsync(await GetUser(), mail);

        public async Task Save(MailIdentity mailId, Mail mail)
            => await mailClient.SetMailAsync(mailId.Guid, mail);

        public async Task<MailStatus> GetStatus(MailIdentity mailId)
            => await mailClient.GetStatusAsync(mailId.Guid);

        internal async Task SendAll()
            => await mailClient.SendAllAsync(await GetUser());

        public async Task<Mail> GetMail(MailIdentity mailId)
            => await mailClient.GetMailAsync(mailId.Guid);

        public async Task<SmtpSettings> GetSmtpSettings()
            => await userClient.GetSmtpSettingsAsync(await GetUser());

        public async Task SetSmtpSettings(SmtpSettings settings)
            => await userClient.SetSmtpSettingsAsync(await GetUser(), settings);

        private async Task<Guid> GetUser()
            => (await user.Value).Guid;
    }
}
