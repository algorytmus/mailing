﻿using System;
using Blazored.LocalStorage;
using System.Threading.Tasks;
using Codibly.Mailing.Web.Client.ServiceReferences;

namespace Codibly.Mailing.Web.Client.Services
{
    public class UserContextProvider
    {
        private static readonly string UserIdLocalKey = "codibly-mailing-app-user-id-2";

        private readonly UserClient userClient;
        private readonly ILocalStorageService localStorage;

        public UserContextProvider(UserClient userClient, ILocalStorageService localStorage)
        {
            this.userClient = userClient;
            this.localStorage = localStorage;
        }

        private async Task<UserIdentity> GetLocalUser()
            => new UserIdentity { Guid = await localStorage.GetItemAsync<Guid>(UserIdLocalKey) };

        private async Task<UserIdentity> CreateUser()
            => await userClient.CreateAsync();

        private async Task<bool> HasLocalUser()
            => await localStorage.ContainKeyAsync(UserIdLocalKey);

        private async Task SaveUser(UserIdentity userId)
            => await localStorage.SetItemAsync(UserIdLocalKey, userId.Guid);

        public async Task<UserIdentity> GetUser()
        {
            if (await HasLocalUser())
            {
                return await GetLocalUser();
            }
            else
            {
                var user = await CreateUser();
                await SaveUser(user);
                return user;
            }
        }
    }
}
