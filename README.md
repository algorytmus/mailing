## I am looking for a job :)

## REST API

```javascript
{
  "x-generator": "NSwag v13.2.5.0 (NJsonSchema v10.1.7.0 (Newtonsoft.Json v9.0.0.0))",
  "swagger": "2.0",
  "info": {
    "title": "My Title",
    "version": "1.0.0"
  },
  "paths": {
    "/{userId}/mails/range/{pageIndex}/{pageSize}": {
      "get": {
        "tags": [
          "Mail"
        ],
        "operationId": "Mail_Range",
        "produces": [
          "text/plain",
          "application/json",
          "text/json"
        ],
        "parameters": [
          {
            "type": "string",
            "name": "userId",
            "in": "path",
            "required": true,
            "format": "guid",
            "x-nullable": false
          },
          {
            "type": "integer",
            "name": "pageIndex",
            "in": "path",
            "required": true,
            "format": "int32",
            "x-nullable": false
          },
          {
            "type": "integer",
            "name": "pageSize",
            "in": "path",
            "required": true,
            "format": "int32",
            "x-nullable": false
          }
        ],
        "responses": {
          "200": {
            "x-nullable": false,
            "description": "",
            "schema": {
              "$ref": "#/definitions/MailInfoPage"
            }
          }
        }
      }
    },
    "/{userId}/mails/drafts": {
      "put": {
        "tags": [
          "Mail"
        ],
        "operationId": "Mail_Create",
        "consumes": [
          "application/json",
          "text/json",
          "application/*+json"
        ],
        "produces": [
          "text/plain",
          "application/json",
          "text/json"
        ],
        "parameters": [
          {
            "type": "string",
            "name": "userId",
            "in": "path",
            "required": true,
            "format": "guid",
            "x-nullable": false
          },
          {
            "name": "mail",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Mail"
            },
            "x-nullable": false
          }
        ],
        "responses": {
          "200": {
            "x-nullable": false,
            "description": "",
            "schema": {
              "$ref": "#/definitions/MailIdentity"
            }
          }
        }
      }
    },
    "/mails/status/{mailId}": {
      "put": {
        "tags": [
          "Mail"
        ],
        "operationId": "Mail_GetStatus",
        "produces": [
          "text/plain",
          "application/json",
          "text/json"
        ],
        "parameters": [
          {
            "type": "string",
            "name": "mailId",
            "in": "path",
            "required": true,
            "format": "guid",
            "x-nullable": false
          }
        ],
        "responses": {
          "200": {
            "x-nullable": false,
            "description": "",
            "schema": {
              "$ref": "#/definitions/MailStatus"
            }
          }
        }
      }
    },
    "/mails/outgoing/{mailId}": {
      "post": {
        "tags": [
          "Mail"
        ],
        "operationId": "Mail_Send",
        "parameters": [
          {
            "type": "string",
            "name": "mailId",
            "in": "path",
            "required": true,
            "format": "guid",
            "x-nullable": false
          }
        ],
        "responses": {
          "200": {
            "description": ""
          }
        }
      }
    },
    "/mails/{mailId}": {
      "get": {
        "tags": [
          "Mail"
        ],
        "operationId": "Mail_GetMail",
        "produces": [
          "text/plain",
          "application/json",
          "text/json"
        ],
        "parameters": [
          {
            "type": "string",
            "name": "mailId",
            "in": "path",
            "required": true,
            "format": "guid",
            "x-nullable": false
          }
        ],
        "responses": {
          "200": {
            "x-nullable": false,
            "description": "",
            "schema": {
              "$ref": "#/definitions/Mail"
            }
          }
        }
      },
      "put": {
        "tags": [
          "Mail"
        ],
        "operationId": "Mail_SetMail",
        "consumes": [
          "application/json",
          "text/json",
          "application/*+json"
        ],
        "parameters": [
          {
            "type": "string",
            "name": "mailId",
            "in": "path",
            "required": true,
            "format": "guid",
            "x-nullable": false
          },
          {
            "name": "mail",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Mail"
            },
            "x-nullable": false
          }
        ],
        "responses": {
          "200": {
            "description": ""
          }
        }
      }
    },
    "/{userId}/mails/smtp": {
      "post": {
        "tags": [
          "Mail"
        ],
        "operationId": "Mail_SendAll",
        "parameters": [
          {
            "type": "string",
            "name": "userId",
            "in": "path",
            "required": true,
            "format": "guid",
            "x-nullable": false
          }
        ],
        "responses": {
          "200": {
            "description": ""
          }
        }
      }
    },
    "/users": {
      "put": {
        "tags": [
          "User"
        ],
        "operationId": "User_Create",
        "produces": [
          "text/plain",
          "application/json",
          "text/json"
        ],
        "responses": {
          "200": {
            "x-nullable": false,
            "description": "",
            "schema": {
              "$ref": "#/definitions/UserIdentity"
            }
          }
        }
      }
    },
    "/users/{userId}/smtp-settings": {
      "put": {
        "tags": [
          "User"
        ],
        "operationId": "User_SetSmtpSettings",
        "consumes": [
          "application/json",
          "text/json",
          "application/*+json"
        ],
        "parameters": [
          {
            "type": "string",
            "name": "userId",
            "in": "path",
            "required": true,
            "format": "guid",
            "x-nullable": false
          },
          {
            "name": "smtpSettings",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/SmtpSettings"
            },
            "x-nullable": false
          }
        ],
        "responses": {
          "200": {
            "description": ""
          }
        }
      }
    }
  },
  "definitions": {
    "MailInfoPage": {
      "type": "object",
      "required": [
        "totalPages"
      ],
      "properties": {
        "mails": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/MailInfo"
          }
        },
        "totalPages": {
          "type": "integer",
          "format": "int32"
        }
      }
    },
    "MailInfo": {
      "type": "object",
      "required": [
        "status"
      ],
      "properties": {
        "identity": {
          "$ref": "#/definitions/MailIdentity"
        },
        "status": {
          "$ref": "#/definitions/MailStatus"
        },
        "header": {
          "type": "string"
        }
      }
    },
    "MailIdentity": {
      "type": "object",
      "required": [
        "guid"
      ],
      "properties": {
        "guid": {
          "type": "string",
          "format": "guid"
        }
      }
    },
    "MailStatus": {
      "type": "integer",
      "description": "",
      "x-enumNames": [
        "Saved",
        "Sending",
        "Sent",
        "Error"
      ],
      "enum": [
        0,
        1,
        2,
        3
      ]
    },
    "Mail": {
      "type": "object",
      "properties": {
        "subject": {
          "type": "string"
        },
        "sender": {
          "type": "string"
        },
        "recipients": {
          "type": "string"
        },
        "body": {
          "type": "string"
        }
      }
    },
    "UserIdentity": {
      "type": "object",
      "required": [
        "guid"
      ],
      "properties": {
        "guid": {
          "type": "string",
          "format": "guid"
        }
      }
    },
    "SmtpSettings": {
      "type": "object",
      "required": [
        "smtpPort"
      ],
      "properties": {
        "smtpAddress": {
          "type": "string"
        },
        "smtpPort": {
          "type": "integer",
          "format": "int32"
        },
        "smtpUser": {
          "type": "string"
        },
        "smtpPassword": {
          "type": "string"
        }
      }
    }
  }
}
```

---